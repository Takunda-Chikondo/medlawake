#!/bin/bash
cd ..
pip3 install -r requirements/production.txt
export DJANGO_SETTINGS_MODULE=medlawake.settings.production
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic

sudo cp /home/deployer/medlawake/server_config/gunicorn_start /home/deployer/bin/gunicorn_start
sudo chmod u+x /home/deployer/bin/gunicorn_start

mkdir /home/deployer/run
mkdir /home/deployer/logs
touch /home/deployer/logs/gunicorn-error.log

sudo cp /home/deployer/medlawake/server_config/medlawake.conf /etc/supervisor/conf.d/medlawake.conf
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl status medlawake
sudo supervisorctl restart medlawake

sudo cp /home/deployer/medlawake/server_config/nginx_config /etc/nginx/sites-available/medlawake
sudo ln -s /etc/nginx/sites-available/medlawake /etc/nginx/sites-enabled/medlawake
sudo rm /etc/nginx/sites-enabled/default
sudo service nginx restart

sudo reboot



