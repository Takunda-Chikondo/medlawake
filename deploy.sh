#!/bin/sh
cd ..
source bin/activate
export DJANGO_SETTINGS_MODULE=medlawake.settings.production
cd medlawake
git pull origin master
pip install -r requirements/production.txt
python manage.py collectstatic --noinput
python manage.py makemigrations
python manage.py migrate
sudo supervisorctl restart medlawake
exit