from django.db import models
from django_cleanup.signals import cleanup_pre_delete
from django.db.models.signals import post_delete

from image_optimizer.fields import OptimizedImageField

class Product(models.Model):
    product_name = models.CharField(max_length=200)
    price = models.CharField(max_length=200)
    product_image = OptimizedImageField(upload_to='product_images/')
    product_description =  models.CharField(max_length=200, default="")
    product_brochure = models.FileField(upload_to='product_specifications/')

    category = models.ForeignKey('Category', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.product_name


class Category(models.Model):
    name = models.CharField(max_length=200, default="")
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

def delete_on_change(**kwargs):
    cleanup_pre_delete.connect(delete_on_change)

def delete_on_delete(**kwargs):
    cleanup_post_delete.connect(delete_on_delete)
