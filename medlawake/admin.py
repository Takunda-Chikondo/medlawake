from django.contrib import admin
from medlawake.models import Product, Category

class ProductAdmin(admin.ModelAdmin):
    list_display = ['product_name', 'price', 'product_description']
    search_fields = ['product_name', 'price']
admin.site.register(Product, ProductAdmin)
admin.site.register(Category)

admin.site.site_header = ("Medlawake Administration")

