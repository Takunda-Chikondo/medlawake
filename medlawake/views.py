from django.http import HttpResponse
from django.template import loader
from .models import Product

context,product_context = {}, {}

def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render({'context': context}, request))

def contact(request):
    template = loader.get_template('contact.html')
    return HttpResponse(template.render({'context': context}, request))

def shop(request):
    context['products'] = Product.objects.all()
    product_context['Solar'] = Product.objects.filter(category__name='Solar Panels').count()
    product_context['Inverter'] = Product.objects.filter(category__name='Inverter').count()
    product_context['Accessories'] = Product.objects.filter(category__name='Accessories').count()
    template = loader.get_template('shop.html')
    return HttpResponse(template.render({'context': context, 'product_context': product_context}, request))
