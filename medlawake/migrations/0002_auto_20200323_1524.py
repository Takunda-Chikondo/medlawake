# Generated by Django 3.0.4 on 2020-03-23 15:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medlawake', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='category',
            new_name='category_id',
        ),
    ]
